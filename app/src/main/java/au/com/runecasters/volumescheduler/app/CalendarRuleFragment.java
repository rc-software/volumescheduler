/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.SchedulerRule;

public class CalendarRuleFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, RuleActivity.ScheduleRuleSetter {
    private Spinner mCalendarList;
    private EditText mTextTitleSearch;
    private EditText mTextLocationSearch;
    private EditText mTextDescSearch;
    private Spinner mAvailabilityList;
    private SimpleCursorAdapter mCursorAdapter;
    private SchedulerRule mSchedulerRule;

    public CalendarRuleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rule_calendar, container, false);
        mSchedulerRule = ((RuleActivity) getActivity()).mSchedulerRule;
        uiSetup(rootView);
        mCursorAdapter = new SimpleCursorAdapter(getActivity(), R.layout.calendar_spinner_item, null,
                new String[] {CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                        CalendarContract.Calendars.ACCOUNT_NAME, CalendarContract.Calendars.ACCOUNT_TYPE},
                new int[] {R.id.calendar_name, R.id.calendar_acct, R.id.calendar_type}, 0);
        mCursorAdapter.setDropDownViewResource(R.layout.calendar_spinner_item);
        mCalendarList.setAdapter(mCursorAdapter);
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(0, null, this);
        return rootView;
    }

    private void uiSetup(View rootView) {
        mCalendarList = (Spinner) rootView.findViewById(R.id.spinnerCalendarList);
        mTextTitleSearch = (EditText) rootView.findViewById(R.id.textTitleSearch);
        mTextLocationSearch = (EditText) rootView.findViewById(R.id.textLocationSearch);
        mTextDescSearch = (EditText) rootView.findViewById(R.id.textDescSearch);
        mAvailabilityList = (Spinner) rootView.findViewById(R.id.spinnerAvailabilityList);
        mCalendarList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long rowID) {
                SchedulerRule schedulerRule = ((RuleActivity) getActivity()).mSchedulerRule;
                schedulerRule.setCalendarID(rowID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Prefill data if this is an edit of an existing rule
        mTextTitleSearch.setText(mSchedulerRule.getSearchTitle());
        mTextLocationSearch.setText(mSchedulerRule.getSearchLocation());
        mTextDescSearch.setText(mSchedulerRule.getSearchDesc());
        mAvailabilityList.setSelection(mSchedulerRule.getAvailability() + 1);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Uri baseUri = CalendarContract.Calendars.CONTENT_URI;
        CursorLoader loader = new CursorLoader(getActivity(), baseUri,
                new String[] {CalendarContract.Calendars._ID, CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                        CalendarContract.Calendars.ACCOUNT_NAME, CalendarContract.Calendars.ACCOUNT_TYPE},
                null, null, "1 ASC");
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);
        if (data.getCount() > 0) {
            RuleActivity activity = (RuleActivity) getActivity();
            activity.ruleUpdate(RuleActivity.TRIGGER_RULE, true);
            long currentCalendarID = mSchedulerRule.getCalendarID();
            if (currentCalendarID >= 0) {
                for (int i = 0; i < data.getCount(); i++) {
                    data.moveToPosition(i);
                    long calendarID = data.getLong(0);
                    if (calendarID == currentCalendarID) {
                        mCalendarList.setSelection(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }

    @Override
    public void setRules(SchedulerRule schedulerRule) {
        schedulerRule.setSearchTitle(mTextTitleSearch.getText().toString());
        schedulerRule.setSearchLocation(mTextLocationSearch.getText().toString());
        schedulerRule.setSearchDesc(mTextDescSearch.getText().toString());
        schedulerRule.setAvailability(mAvailabilityList.getSelectedItemPosition() - 1);
    }
}
