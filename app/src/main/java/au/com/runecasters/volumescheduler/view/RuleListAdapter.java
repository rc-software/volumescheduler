/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.SchedulerRule;

import java.util.List;

public class RuleListAdapter extends ArrayAdapter<SchedulerRule> {
    private int mSelectedItemPos = -1;
    private int mSelectedRuleColour;
    private int mUnselectedRuleColour;

    public RuleListAdapter(Context context, List<SchedulerRule> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        mSelectedRuleColour = context.getResources().getColor(R.color.selectedRuleColour);
        mUnselectedRuleColour = context.getResources().getColor(R.color.unselectedRuleColour);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            if (position == mSelectedItemPos) {
                convertView.setBackgroundColor(mSelectedRuleColour);
            } else {
                convertView.setBackgroundColor(mUnselectedRuleColour);
            }
        }
        return super.getView(position, convertView, parent);
    }

    public void setSelectedItem(int position) {
        mSelectedItemPos = position;
        notifyDataSetChanged();
    }

    @Override
    public void remove(SchedulerRule object) {
        super.remove(object);
        mSelectedItemPos = -1;
    }

    @Override
    public void insert(SchedulerRule object, int index) {
        super.insert(object, index);
        mSelectedItemPos = index;
        notifyDataSetChanged();
    }

    @Override
    public void clear() {
        super.clear();
        mSelectedItemPos = -1;
    }
}
