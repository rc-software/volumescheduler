/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.SchedulerRule;
import au.com.runecasters.volumescheduler.view.DialogFragments;

public class TimeRuleFragment extends Fragment implements RuleActivity.ScheduleRuleSetter {
    private static final int REQUEST_START = 0;
    private static final int REQUEST_END = 1;
    private Button mButtonSetStart;
    private Button mButtonSetEnd;
    private CheckBox[] mDaysOfWeek;
    private SchedulerRule mSchedulerRule;
    private int[] mStartTime;
    private int[] mEndTime;

    public TimeRuleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rule_time, container, false);
        mSchedulerRule = ((RuleActivity) getActivity()).mSchedulerRule;
        uiSetup(rootView);
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            int hour = data.getIntExtra("hour", 0);
            int minute = data.getIntExtra("minute", 0);
            String buttonLabel = String.format("%02d:%02d", hour, minute);
            if (requestCode == REQUEST_START) {
                mStartTime = new int[]{hour, minute};
                mButtonSetStart.setText(buttonLabel);
            } else if (requestCode == REQUEST_END) {
                mEndTime = new int[]{hour, minute};
                mButtonSetEnd.setText(buttonLabel);
            }
            if (mStartTime != null && mEndTime != null) {
                onChange();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uiSetup(View rootView) {
        mButtonSetStart = (Button) rootView.findViewById(R.id.buttonSetStartTime);
        mButtonSetEnd = (Button) rootView.findViewById(R.id.buttonSetEndTime);
        mDaysOfWeek = new CheckBox[7];
        mDaysOfWeek[0] = (CheckBox) rootView.findViewById(R.id.checkBoxSun);
        mDaysOfWeek[1] = (CheckBox) rootView.findViewById(R.id.checkBoxMon);
        mDaysOfWeek[2] = (CheckBox) rootView.findViewById(R.id.checkBoxTues);
        mDaysOfWeek[3] = (CheckBox) rootView.findViewById(R.id.checkBoxWed);
        mDaysOfWeek[4] = (CheckBox) rootView.findViewById(R.id.checkBoxThurs);
        mDaysOfWeek[5] = (CheckBox) rootView.findViewById(R.id.checkBoxFri);
        mDaysOfWeek[6] = (CheckBox) rootView.findViewById(R.id.checkBoxSat);

        View.OnClickListener timeButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int requestCode = -1;
                Bundle currentSetTime = new Bundle();
                if (view == mButtonSetStart) {
                    requestCode = REQUEST_START;
                    currentSetTime.putIntArray("setTime", mStartTime);
                } else if (view == mButtonSetEnd) {
                    requestCode = REQUEST_END;
                    currentSetTime.putIntArray("setTime", mEndTime);
                }
                DialogFragment timePickerFragment = new DialogFragments.TimePickerFragment();
                timePickerFragment.setArguments(currentSetTime);
                timePickerFragment.setTargetFragment(getSelfReference(), requestCode);
                timePickerFragment.show(getFragmentManager(), "Time Selector");
            }
        };
        mButtonSetStart.setOnClickListener(timeButtonOnClickListener);
        mButtonSetEnd.setOnClickListener(timeButtonOnClickListener);

        CompoundButton.OnCheckedChangeListener daysCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                onChange();
            }
        };
        for (CheckBox checkDay : mDaysOfWeek) {
            checkDay.setOnCheckedChangeListener(daysCheckedChangeListener);
        }

        // Prefill data if this is an edit of an existing rule
        mStartTime = mSchedulerRule.getStartTime();
        if (mStartTime != null) mButtonSetStart.setText(String.format("%02d:%02d", mStartTime[0], mStartTime[1]));
        mEndTime = mSchedulerRule.getEndTime();
        if (mEndTime != null) mButtonSetEnd.setText(String.format("%02d:%02d", mEndTime[0], mEndTime[1]));
        boolean[] scheduledDays = mSchedulerRule.getDaysOfWeek();
        if (scheduledDays != null) for (int i = 0; i < 7; i++) {
            mDaysOfWeek[i].setChecked(scheduledDays[i]);
        }
    }

    private Fragment getSelfReference() {
        // Horrible hack to get fragment reference for DialogFragment, don't judge me
        return this;
    }

    @Override
    public void setRules(SchedulerRule schedulerRule) {
        boolean[] scheduledDays = new boolean[7];
        for (int i = 0; i < 7; i++) {
            scheduledDays[i] = mDaysOfWeek[i].isChecked();
        }
        schedulerRule.setDaysOfWeek(scheduledDays);
        schedulerRule.setStartTime(mStartTime);
        schedulerRule.setEndTime(mEndTime);
    }

    private void onChange() {
        boolean daySelected = false;
        for(CheckBox checkDay : mDaysOfWeek) {
            if (checkDay.isChecked()) {
                daySelected = true;
                break;
            }
        }
        if ((mStartTime != null && mEndTime != null) && daySelected) {
            ((RuleActivity) getActivity()).ruleUpdate(RuleActivity.TRIGGER_RULE, true);
        } else {
            ((RuleActivity) getActivity()).ruleUpdate(RuleActivity.TRIGGER_RULE, false);
        }
    }
}
