/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import au.com.runecasters.volumescheduler.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Timer;
import java.util.TimerTask;

public class AdFragment extends Fragment{
    private AdView mAdView;
    private Timer mAdReloadTimer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ad, container, false);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        mAdView = (AdView) getView().findViewById(R.id.adView);
        final AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mAdView.setVisibility(View.GONE);
                    // Ads never resume by themselves because they're invisible, so make our own timer instead
                    if (mAdReloadTimer == null) {
                        mAdReloadTimer = new Timer();
                        mAdReloadTimer.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                try {
                                    getActivity().runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            mAdView.loadAd(adRequest);
                                        }
                                    });
                                } catch (NullPointerException e) {
                                    // Throw away exception because it's likely caused by rapid device rotation
                                }
                            }
                        }, 30000);
                    }
                }

                @Override
                public void onAdLoaded() {
                    mAdView.setVisibility(View.VISIBLE);
                    // Kill any forced resume timer since it SHOULDN'T be needed anymore
                    if (mAdReloadTimer != null) {
                        mAdReloadTimer.cancel();
                        mAdReloadTimer.purge();
                        mAdReloadTimer = null;
                    }
                    super.onAdLoaded();
                }
            });

    }

    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}
