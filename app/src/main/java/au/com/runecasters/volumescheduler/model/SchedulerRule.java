/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

public class SchedulerRule implements Parcelable{
    public static final int CALENDAR_RULE = 0;
    public static final int TIME_RULE = 1;
    public static final Parcelable.Creator<SchedulerRule> CREATOR = new Parcelable.Creator<SchedulerRule>() {
        public SchedulerRule createFromParcel(Parcel in) {
            return new SchedulerRule(in);
        }

        @Override
        public SchedulerRule[] newArray(int size) {
            return new SchedulerRule[size];
        }
    };

    private final long mRuleID;
    private final int mRuleType;
    private int mVolRingtone = -1;
    private boolean mVibrate = false;
    private int mVolNotification = -1;
    private int mVolMedia = -1;
    private int mVolAlarm = -1;
    private String mRuleName = "";

    private long mCalendarID = -1;
    private String mSearchTitle = "";
    private String mSearchLocation = "";
    private String mSearchDesc = "";
    private int mAvailability = -1;

    private int[] mStartTime;
    private int[] mEndTime;
    private boolean[] mDaysOfWeek;

    public SchedulerRule(int mRuleType) {
        this(mRuleType, -1);
    }

    public SchedulerRule(int mRuleType, long mRuleID) {
        this.mRuleType = mRuleType;
        this.mRuleID = mRuleID;
    }

    public SchedulerRule(Parcel in) {
        mRuleID = in.readLong();
        mRuleType = in.readInt();
        mVolRingtone = in.readInt();
        mVibrate = in.readByte() != 0;
        mVolNotification = in.readInt();
        mVolMedia = in.readInt();
        mVolAlarm = in.readInt();
        mRuleName = in.readString();

        if (mRuleType == CALENDAR_RULE) {
            mCalendarID = in.readLong();
            mSearchTitle = in.readString();
            mSearchLocation = in.readString();
            mSearchDesc = in.readString();
            mAvailability = in.readInt();
        } else if (mRuleType == TIME_RULE) {
            mStartTime = in.createIntArray();
            mEndTime = in.createIntArray();
            mDaysOfWeek = in.createBooleanArray();
        }
    }

    public long getRuleID() {
        return mRuleID;
    }

    public int getRuleType() {
        return mRuleType;
    }

    // Calendar rule getters/setters
    public long getCalendarID() {
        return mCalendarID;
    }

    public void setCalendarID(long calendarID) {
        mCalendarID = calendarID;
    }

    public String getSearchTitle() {
        return mSearchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        mSearchTitle = searchTitle;
    }

    public String getSearchLocation() {
        return mSearchLocation;
    }

    public void setSearchLocation(String searchLocation) {
        mSearchLocation = searchLocation;
    }

    public String getSearchDesc() {
        return mSearchDesc;
    }

    public void setSearchDesc(String searchDesc) {
        mSearchDesc = searchDesc;
    }

    public int getAvailability() {
        return mAvailability;
    }

    public void setAvailability(int availability) {
        mAvailability = availability;
    }

    // Volume rule getters/setters
    public boolean changingRingtone() {
        return mVolRingtone >= 0;
    }

    public void changeRingtone(boolean changeRingtone, int volume) {
        if (changeRingtone) {
            mVolRingtone = volume;
        } else {
            mVolRingtone = -1;
        }
    }

    public boolean isVibrate() {
        return mVibrate;
    }

    public void setVibrate(boolean vibrate) {
        mVibrate = vibrate;
    }

    public boolean changingNotification() {
        return mVolNotification >= 0;
    }

    public void changeNotification(boolean changeNotification, int volume) {
        if (changeNotification) {
            mVolNotification = volume;
        } else {
            mVolNotification = -1;
        }
    }

    public boolean changingMedia() {
        return mVolMedia >= 0;
    }

    public void changeMedia(boolean changeMedia, int volume) {
        if (changeMedia) {
            mVolMedia = volume;
        } else {
            mVolMedia = -1;
        }
    }

    public boolean changingAlarm() {
        return mVolAlarm >= 0;
    }

    public void changeAlarm(boolean changeAlarm, int volume) {
        if (changeAlarm) {
            mVolAlarm = volume;
        } else {
            mVolAlarm = -1;
        }
    }

    public int getVolRingtone() {
        return mVolRingtone;
    }

    public int getVolNotification() {
        return mVolNotification;
    }

    public int getVolMedia() {
        return mVolMedia;
    }

    public int getVolAlarm() {
        return mVolAlarm;
    }

    public void setRuleName(String ruleName) {
        mRuleName = ruleName;
    }

    // Time rule getters/setters

    public int[] getStartTime() {
        return mStartTime;
    }

    public void setStartTime(int[] startTime) {
        if (startTime != null) mStartTime = startTime;
    }

    public int[] getEndTime() {
        return mEndTime;
    }

    public void setEndTime(int[] endTime) {
        if (endTime != null) mEndTime = endTime;
    }

    public boolean[] getDaysOfWeek() {
        return mDaysOfWeek;
    }

    public void setDaysOfWeek(boolean[] daysOfWeek) {
        mDaysOfWeek = daysOfWeek;
    }

    public String serialiseDaysOfWeek() {
        String serialised = "";
        for (boolean day : mDaysOfWeek) {
            serialised += day ? 'Y' : 'N';
        }
        return serialised;
    }

    public void setDaysOfWeekFromSerial(String serialised) {
        mDaysOfWeek = new boolean[7];
        for (int i = 0; i < 7; i++) {
            mDaysOfWeek[i] = (serialised.charAt(i) == 'Y');
        }
    }

    // Parcelable implements
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mRuleID);
        dest.writeInt(mRuleType);
        dest.writeInt(mVolRingtone);
        dest.writeByte((byte) (mVibrate ? 1 : 0));
        dest.writeInt(mVolNotification);
        dest.writeInt(mVolMedia);
        dest.writeInt(mVolAlarm);
        dest.writeString(mRuleName);

        if (mRuleType == CALENDAR_RULE) {
            dest.writeLong(mCalendarID);
            dest.writeString(mSearchTitle);
            dest.writeString(mSearchLocation);
            dest.writeString(mSearchDesc);
            dest.writeInt(mAvailability);
        } else if (mRuleType == TIME_RULE) {
            dest.writeIntArray(mStartTime);
            dest.writeIntArray(mEndTime);
            dest.writeBooleanArray(mDaysOfWeek);
        }
    }

    @Override
    public String toString() {
        return mRuleName;
    }

    public ContentValues getBaseRuleValues() {
        ContentValues ruleVals = new ContentValues(8);
        ruleVals.put("ruleType", mRuleType);
        ruleVals.put("ruleName", mRuleName);
        ruleVals.put("volRingtone", mVolRingtone);
        ruleVals.put("volNotification", mVolNotification);
        ruleVals.put("volMedia", mVolMedia);
        ruleVals.put("volAlarm", mVolAlarm);
        ruleVals.put("vibrate", mVibrate ? 1 : 0);
        return ruleVals;
    }

    public ContentValues getCalendarRuleValues() {
        ContentValues calendarVals = new ContentValues(6);
        calendarVals.put("ruleID", mRuleID);
        calendarVals.put("calendarID", mCalendarID);
        calendarVals.put("searchTitle", mSearchTitle);
        calendarVals.put("searchLocation", mSearchLocation);
        calendarVals.put("searchDesc", mSearchDesc);
        calendarVals.put("availability", mAvailability);
        return calendarVals;
    }

    public ContentValues getTimeDayRuleValues() {
        ContentValues timeDayVals = new ContentValues(6);
        timeDayVals.put("ruleID", mRuleID);
        timeDayVals.put("startHour", mStartTime[0]);
        timeDayVals.put("startMinute", mStartTime[1]);
        timeDayVals.put("endHour", mEndTime[0]);
        timeDayVals.put("endMinute", mEndTime[1]);
        timeDayVals.put("daysOfWeek", serialiseDaysOfWeek());
        return timeDayVals;
    }
}
