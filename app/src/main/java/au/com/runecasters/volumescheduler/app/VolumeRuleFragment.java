/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.Fragment;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.SchedulerRule;

public class VolumeRuleFragment extends Fragment implements RuleActivity.ScheduleRuleSetter {
    private CheckBox mCheckRingtoneVol;
    private CheckBox mCheckVibrate;
    private CheckBox mCheckMediaVol;
    private CheckBox mCheckAlarmVol;
    private SeekBar mSeekBarRingtoneVol;
    private SeekBar mSeekBarMediaVol;
    private SeekBar mSeekBarAlarmVol;
    private EditText mTextRuleName;
    private SchedulerRule mSchedulerRule;

    public VolumeRuleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rule_volume, container, false);
        mSchedulerRule = ((RuleActivity) getActivity()).mSchedulerRule;
        uiSetup(rootView);
        return rootView;
    }

    private void uiSetup(View rootView) {
        mCheckRingtoneVol = (CheckBox) rootView.findViewById(R.id.checkBoxRingtoneVol);
        mCheckVibrate = (CheckBox) rootView.findViewById(R.id.checkBoxVibrate);
        mCheckMediaVol = (CheckBox) rootView.findViewById(R.id.checkBoxMediaVol);
        mCheckAlarmVol = (CheckBox) rootView.findViewById(R.id.checkBoxAlarmVol);
        mSeekBarRingtoneVol = (SeekBar) rootView.findViewById(R.id.seekBarRingtoneVol);
        mSeekBarMediaVol = (SeekBar) rootView.findViewById(R.id.seekBarMediaVol);
        mSeekBarAlarmVol = (SeekBar) rootView.findViewById(R.id.seekBarAlarmVol);
        mTextRuleName = (EditText) rootView.findViewById(R.id.textRuleName);

        mSeekBarAlarmVol.setEnabled(false);
        mSeekBarRingtoneVol.setEnabled(false);
        mSeekBarMediaVol.setEnabled(false);

        mCheckRingtoneVol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mSeekBarRingtoneVol.setEnabled(isChecked);
                mCheckVibrate.setEnabled(isChecked);
                onChange();
            }
        });
        mCheckMediaVol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mSeekBarMediaVol.setEnabled(isChecked);
                onChange();
            }
        });
        mCheckAlarmVol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mSeekBarAlarmVol.setEnabled(isChecked);
                onChange();
            }
        });
        mTextRuleName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                onChange();
            }
        });

        // Set volume sliders to current settings
        AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mSeekBarRingtoneVol.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_RING));
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE ||
                Settings.System.getInt(getActivity().getContentResolver(), "vibrate_when_ringing", 0) == 1) {
            mCheckVibrate.setChecked(true);
        }
        mSeekBarMediaVol.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        mSeekBarAlarmVol.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_ALARM));

        // Prefill data if this is an edit of an existing rule
        if (mSchedulerRule.changingRingtone()) {
            mCheckRingtoneVol.setChecked(true);
            mSeekBarRingtoneVol.setProgress(mSchedulerRule.getVolRingtone());
            mCheckVibrate.setChecked(mSchedulerRule.isVibrate());
        }
        if (mSchedulerRule.changingMedia()) {
            mCheckMediaVol.setChecked(true);
            mSeekBarMediaVol.setProgress(mSchedulerRule.getVolMedia());
        }
        if (mSchedulerRule.changingAlarm()) {
            mCheckAlarmVol.setChecked(true);
            mSeekBarAlarmVol.setProgress(mSchedulerRule.getVolAlarm());
        }
        mTextRuleName.setText(mSchedulerRule.toString());
    }

    private void onChange() {
        if (mTextRuleName.length() > 0 &&
                (mCheckRingtoneVol.isChecked() ||
                        mCheckMediaVol.isChecked() ||
                        mCheckAlarmVol.isChecked())) {
            ((RuleActivity) getActivity()).ruleUpdate(RuleActivity.VOLUME_RULE, true);
        } else {
            ((RuleActivity) getActivity()).ruleUpdate(RuleActivity.VOLUME_RULE, false);
        }
    }

    @Override
    public void setRules(SchedulerRule schedulerRule) {
        schedulerRule.changeRingtone(mCheckRingtoneVol.isChecked(), mSeekBarRingtoneVol.getProgress());
        schedulerRule.setVibrate(mCheckRingtoneVol.isChecked() && mCheckVibrate.isChecked());
        // Unless I find a way of splitting ringtone/notification volumes consistently across roms,
        // store notification volume the same as ringtone volume for future-backwards compatibility
        schedulerRule.changeNotification(mCheckRingtoneVol.isChecked(), mSeekBarRingtoneVol.getProgress());
        schedulerRule.changeMedia(mCheckMediaVol.isChecked(), mSeekBarMediaVol.getProgress());
        schedulerRule.changeAlarm(mCheckAlarmVol.isChecked(), mSeekBarAlarmVol.getProgress());
        schedulerRule.setRuleName(mTextRuleName.getText().toString());
    }
}
