/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.view;

import android.app.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.SchedulerRule;

public class DialogFragments {

    private static DialogInterface.OnClickListener sCancelDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };

    public static class NewRuleSelectorDialogFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final MainActivityDialogListener activity = (MainActivityDialogListener) getActivity();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.dialog_newRuleSelector)
                    .setPositiveButton(R.string.dialogBtn_timeSelector, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.startRuleActivity(SchedulerRule.TIME_RULE);
                        }
                    })
                    .setNeutralButton(R.string.dialogBtn_calendarSelector, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.startRuleActivity(SchedulerRule.CALENDAR_RULE);
                        }
                    })
                    .setNegativeButton(R.string.dialogBtn_cancel, sCancelDialog);
            return builder.create();
        }
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int[] currentlySetTime = getArguments().getIntArray("setTime");
            if (currentlySetTime == null) {
                currentlySetTime = new int[] {0,0};
            }
            return new TimePickerDialog(getActivity(), this, currentlySetTime[0], currentlySetTime[1],
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Intent data = new Intent();
            data.putExtra("hour", hourOfDay);
            data.putExtra("minute", minute);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
            dismiss();
        }
    }

    public static class DeleteRuleConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final MainActivityDialogListener activity = (MainActivityDialogListener) getActivity();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure you want to delete this rule?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.deleteSelectedRule();
                        }
                    })
                    .setNegativeButton("No", sCancelDialog);
            return builder.create();
        }
    }

    public interface MainActivityDialogListener {
        void startRuleActivity(int selector);
        void deleteSelectedRule();
    }
}
