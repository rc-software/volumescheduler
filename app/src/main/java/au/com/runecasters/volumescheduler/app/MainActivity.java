/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.DatabaseHelper;
import au.com.runecasters.volumescheduler.model.SchedulerRule;
import au.com.runecasters.volumescheduler.service.VolumeSchedulerService.VolumeSchedulerReceiver;
import au.com.runecasters.volumescheduler.view.DialogFragments;
import au.com.runecasters.volumescheduler.view.RuleListAdapter;

import java.util.ArrayList;


public class MainActivity extends Activity implements DialogFragments.MainActivityDialogListener {
    public static final String TAG = "Activity";
    private RuleListAdapter mRuleListAdapter;
    private DatabaseHelper mDbHelper;
    private SharedPreferences mSharedPrefs;
    private ComponentName mVolumeSchedulerReceiver;
    private boolean mEnableTriggers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = DatabaseHelper.getInstance(this);
        mRuleListAdapter = new RuleListAdapter(this, mDbHelper.getRules());
        mSharedPrefs = getSharedPreferences("VolumeScheduler", MODE_PRIVATE);
        mVolumeSchedulerReceiver = new ComponentName(this, VolumeSchedulerReceiver.class);
        boolean firstRun = mSharedPrefs.getBoolean("firstRun", true);
        if (firstRun) {
            toggleTriggers(true);
            SharedPreferences.Editor editor = mSharedPrefs.edit();
            editor.putBoolean("firstRun", false);
            editor.apply();
        }
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mEnableTriggers = mSharedPrefs.getBoolean("enableTriggers", true);
        if (!mEnableTriggers) {
            MenuItem serviceToggle = menu.findItem(R.id.toggle_service);
            serviceToggle.setTitle(getResources().getString(R.string.menu_toggleDisabled));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.toggle_service:
                if (mEnableTriggers) {
                    mEnableTriggers = false;
                    toggleTriggers(false);
                    item.setTitle(getResources().getString(R.string.menu_toggleDisabled));
                } else {
                    mEnableTriggers = true;
                    toggleTriggers(true);
                    item.setTitle(getResources().getString(R.string.menu_toggleEnabled));
                }
                break;
            case R.id.menu_about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleTriggers(boolean enable) {
        Intent intent = new Intent(this, VolumeSchedulerReceiver.class);
        intent.setAction(enable ? VolumeSchedulerReceiver.START_TRIGGERS : VolumeSchedulerReceiver.STOP_TRIGGERS);
        if (enable) {
            getPackageManager().setComponentEnabledSetting(mVolumeSchedulerReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            sendBroadcast(intent);
        } else {
            sendBroadcast(intent);
            getPackageManager().setComponentEnabledSetting(mVolumeSchedulerReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        }
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putBoolean("enableTriggers", enable);
        editor.apply();
    }

    @Override
    public void startRuleActivity(int selector) {
        Intent ruleIntent = new Intent(this, RuleActivity.class);
        ruleIntent.putExtra("selector", selector);
        startActivityForResult(ruleIntent, 0);
    }

    private void startRuleActivityForEdit() {
        ListView ruleList = (ListView) findViewById(R.id.listViewRuleList);
        SchedulerRule selectedRule = mRuleListAdapter.getItem(ruleList.getCheckedItemPosition());
        Intent ruleIntent = new Intent(this, RuleActivity.class);
        ruleIntent.putExtra("selector", selectedRule.getRuleType());
        ruleIntent.putExtra("existingRule", selectedRule);
        startActivityForResult(ruleIntent, 0);
    }

    @Override
    public void deleteSelectedRule() {
        ListView ruleList = (ListView) findViewById(R.id.listViewRuleList);
        SchedulerRule selectedRule = mRuleListAdapter.getItem(ruleList.getCheckedItemPosition());
        mDbHelper.delRule(selectedRule.getRuleID());
        mRuleListAdapter.remove(selectedRule);
        MainActivityFragment fragment = (MainActivityFragment) getFragmentManager().findFragmentById(R.id.mainFragment);
        fragment.clearRuleListSelection();
        triggerServiceUpdate();
    }

    private void changeRulePriority(int priorityChange) {
        ListView ruleList = (ListView) findViewById(R.id.listViewRuleList);
        int currentPos = ruleList.getCheckedItemPosition();
        int newPos = currentPos + priorityChange;
        if (newPos >= 0 && newPos < ruleList.getCount()) {
            SchedulerRule selectedRule = mRuleListAdapter.getItem(currentPos);
            mRuleListAdapter.remove(selectedRule);
            mRuleListAdapter.insert(selectedRule, newPos);
            mDbHelper.changeRulePriority(selectedRule.getRuleID(), priorityChange);
            MainActivityFragment fragment = (MainActivityFragment) getFragmentManager().findFragmentById(R.id.mainFragment);
            fragment.updateSelectionAfterMove(newPos);
            triggerServiceUpdate();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MainActivityFragment fragment = (MainActivityFragment) getFragmentManager().findFragmentById(R.id.mainFragment);
        fragment.clearRuleListSelection();
        if (resultCode == RESULT_OK) {
            mRuleListAdapter.clear();
            mRuleListAdapter.addAll(mDbHelper.getRules());
            mRuleListAdapter.notifyDataSetChanged();
            triggerServiceUpdate();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void triggerServiceUpdate() {
        Intent updateIntent = new Intent(this, VolumeSchedulerReceiver.class);
        updateIntent.setAction(VolumeSchedulerReceiver.UPDATE_TRIGGERS);
        sendBroadcast(updateIntent);
    }

    public static class MainActivityFragment extends Fragment {
        public static final int BUTTONSET_ADD = 0;
        public static final int BUTTONSET_EDIT = 1;
        private boolean mItemSelected;
        private int mItemSelectedPos;
        private ListView mRuleList;
        private RuleListAdapter mRuleListAdapter;

        public MainActivityFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setupInterface();
        }

        private void changeButtons(int buttonSet) {
            ArrayList<View> editButtons = new ArrayList<View>(4);
            editButtons.add(getView().findViewById(R.id.buttonUpPriority));
            editButtons.add(getView().findViewById(R.id.buttonDnPriority));
            editButtons.add(getView().findViewById(R.id.buttonEditRule));
            editButtons.add(getView().findViewById(R.id.buttonDelRule));
            View buttonAddRule = getView().findViewById(R.id.buttonAddRule);
            switch (buttonSet) {
                case BUTTONSET_ADD:
                    buttonAddRule.setVisibility(View.VISIBLE);
                    buttonAddRule.setClickable(true);
                    for (View button : editButtons) {
                        button.setVisibility(View.INVISIBLE);
                        button.setClickable(false);
                    }
                    break;
                case BUTTONSET_EDIT:
                    buttonAddRule.setVisibility(View.INVISIBLE);
                    buttonAddRule.setClickable(false);
                    for (View button : editButtons) {
                        button.setVisibility(View.VISIBLE);
                        button.setClickable(true);
                    }
            }
        }

        private void setupInterface() {
            // Set up button listeners
            Button buttonAddRule = (Button) getView().findViewById(R.id.buttonAddRule);
            buttonAddRule.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogFragment selectorDialog = new DialogFragments.NewRuleSelectorDialogFragment();
                    selectorDialog.show(getFragmentManager(), "New Rule Selector");
                }
            });
            Button buttonUpPriority = (Button) getView().findViewById(R.id.buttonUpPriority);
            buttonUpPriority.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).changeRulePriority(-1);
                }
            });
            Button buttonDnPriority = (Button) getView().findViewById(R.id.buttonDnPriority);
            buttonDnPriority.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).changeRulePriority(1);
                }
            });
            Button buttonEditRule = (Button) getView().findViewById(R.id.buttonEditRule);
            buttonEditRule.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).startRuleActivityForEdit();
                }
            });
            Button buttonDelRule = (Button) getView().findViewById(R.id.buttonDelRule);
            buttonDelRule.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogFragment deleteRuleFragment = new DialogFragments.DeleteRuleConfirmationDialog();
                    deleteRuleFragment.show(getFragmentManager(), "Delete Rule Confirmation");
                }
            });

            // Set up rule ListView
            mRuleListAdapter = ((MainActivity) getActivity()).mRuleListAdapter;
            mRuleList = (ListView) getView().findViewById(R.id.listViewRuleList);
            mRuleList.setAdapter(mRuleListAdapter);
            mRuleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long rowID) {
                    if (!mItemSelected) {
                        mItemSelected = true;
                        mItemSelectedPos = position;
                        mRuleListAdapter.setSelectedItem(mItemSelectedPos);
                        changeButtons(BUTTONSET_EDIT);
                    } else if (mItemSelectedPos == position) {
                        clearRuleListSelection();
                    } else {
                        mItemSelectedPos = position;
                        mRuleListAdapter.setSelectedItem(position);
                    }
                }
            });
            getActivity().findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearRuleListSelection();
                }
            });
        }

        private void clearRuleListSelection() {
            mRuleListAdapter.setSelectedItem(-1);
            mRuleList.clearChoices();
            mRuleList.requestLayout();
            changeButtons(BUTTONSET_ADD);
            mItemSelectedPos = -1;
            mItemSelected = false;
        }

        private void updateSelectionAfterMove(int newPos) {
            mRuleList.setItemChecked(newPos, true);
            mItemSelectedPos = newPos;
        }
    }
}
